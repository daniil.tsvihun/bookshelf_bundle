<?php

namespace DaniilTsv\BookshelfBundle\Dto;

use Symfony\Component\HttpFoundation\Request;

interface RequestResolvableInterface
{
    public static function makeFromRequest(Request $request): self;
}