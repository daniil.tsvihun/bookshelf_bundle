<?php

declare(strict_types=1);

namespace DaniilTsv\BookshelfBundle\Dto;

use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class BookDto implements RequestResolvableInterface
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Type('string')]
        #[Assert\Length(max: 30)]
        #[Assert\Regex('/^[a-zA-Z\d ]+$/')]
        public readonly ?string $title,

        #[Assert\NotBlank]
        #[Assert\Type('string')]
        #[Assert\Length(max: 30)]
        #[Assert\Regex('/^[a-zA-Z\d ]+$/')]
        public readonly ?string $author,

        #[Assert\Range(min: 0, max: 1000)]
        #[Assert\NotBlank]
        #[Assert\Type('integer')]
        public readonly ?int $pages,

        #[Assert\DateTime(format: 'd-m-Y')]
        #[Assert\NotBlank]
        public readonly ?string $releaseDate,
    )
    {
    }

    #[Assert\NotBlank]
    #[Assert\LessThanOrEqual('+100 years')]
    #[Assert\GreaterThanOrEqual('-100 years')]
    public function getReleaseDate(): ?DateTimeImmutable
    {
        if ($this->releaseDate) {
            return DateTimeImmutable::createFromFormat('d-m-Y', $this->releaseDate) ?: null;
        }
        return null;
    }

    public static function makeFromRequest(Request $request): self
    {
        $body = $request->toArray();
        return new self(
            title: isset($body['title']) && is_string($body['title']) ? $body['title'] : null,
            author: isset($body['author']) && is_string($body['author']) ? $body['author'] : null,
            pages: isset($body['pages']) && is_int($body['pages']) ? $body['pages'] : null,
            releaseDate: isset($body['releaseDate']) && is_string($body['releaseDate']) ? $body['releaseDate'] : null,
        );
    }
}