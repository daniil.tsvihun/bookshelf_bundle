<?php

declare(strict_types=1);

namespace DaniilTsv\BookshelfBundle;

use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class BookshelfBundle extends AbstractBundle
{
}